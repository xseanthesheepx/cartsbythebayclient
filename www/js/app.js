//Carts by the bay client app
//v 1.0
//Sean Sullivan

angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
 
  $stateProvider
  .state('orders', {
    url: '/',
    templateUrl: 'templates/orders.html',
    controller: 'OrdersCtrl'
  })
 
 
  $urlRouterProvider.otherwise("/");
})



.controller('OrdersCtrl', function($scope, $state, $ionicPopup) {
  var options = {timeout: 10000, enableHighAccuracy: true};
    
//Read data from database
    var db = new Firebase('https://carts-by-the-bay.firebaseio.com/');
    var routes = db.ref();
        routes.on('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
        var childData = childSnapshot.val();
            
//Parse data            
            var nameData = JSON.stringify(childData[Object.keys(childData)[0]].Name);
            var locationDataInfo = JSON.stringify(childData[Object.keys(childData)[0]].Location);
            var destinationData = JSON.stringify(childData[Object.keys(childData)[0]].Destination);
            var pickUpData = JSON.stringify(childData[Object.keys(childData)[0]].PickUp);
            var dropOffData = JSON.stringify(childData[Object.keys(childData)[0]].DropOff);
//Remove Quotes
            var locationInfo = locationDataInfo.replace(/\"/g, "");
            var destination = destinationData.replace(/\"/g, "");
            var name = nameData.replace(/\"/g, "");
            var pickUp = pickUpData.replace(/\"/g,"");
            var dropOff = dropOffData.replace(/\"/g,"");
            
//Print Name & Number            
            document.getElementById('name').innerHTML =  "NAME: " + name;
            document.getElementById('location').innerHTML = "PICKUP LOCATION: " + locationInfo;
            document.getElementById('destination').innerHTML = "DROPOFF LOCATION: " + destination;
            
    
//Store Data Temporarily            
            localStorage.setItem('pickUp', pickUp);
            localStorage.setItem('dropOff', dropOff);          
           
    });
    
       
});
    
    
 
 //Click Name to Open Map
    var open = document.getElementById('open');
        open.addEventListener('click',  function initMap() {
 //Use stored place IDs and get route          
        var pickUp = localStorage.getItem('pickUp');
        var dropOff = localStorage.getItem('dropOff');
        
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
          mapTypeControl: false,
          center: {lat: 30.5230, lng: -87.9033},
          zoom: 13
        });
      
    //place markers on pickup and dropoff locations        
        var service = new google.maps.places.PlacesService(map);
            service.getDetails({
            placeId: pickUp
            }, function (result, status) {
            var marker = new google.maps.Marker({
                map: map,
                icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                place: {
                    placeId: pickUp,
                    location: result.geometry.location
                    }
                });  
            });
            service.getDetails({
                placeId: dropOff
            }, function (results, status) {
                var marker2 = new google.maps.Marker({
                    map: map,
                    place: {
                        placeId: dropOff,
                        location: results.geometry.location
                    }
                });
            });
  
   });

    
    // Delete Order    
   
        var deleteData = document.getElementById('delete');
        deleteData.addEventListener('click', function deleteData() {
        
       var ref = new Firebase('https://carts-by-the-bay.firebaseio.com/');
      
          
        var confirmPopup = $ionicPopup.confirm({
            title: 'Delete',
            template: 'Delete This Order?'
            });

   confirmPopup.then(function(res) {
     if(res) {
       ref.child("Route").child('-KZRyKNGguz3qafk6IFM');
         
         
       window.location.reload();
     } else {
       return;
     }
   });
        
   
    }); 
 
/* close map
    
    var close = document.getElementById('close');
        close.addEventListener('click',  function initMap() {
      window.location.reload(1);
        }); */
});

//TODO
 
  //Possible notification to user
  //send orders to permanent database
  //phone notifications on new order 
